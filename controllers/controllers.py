# -*- coding: utf-8 -*-
from flectra import http
from flectra.http import request
from flectra.addons.website_sale.controllers.main import WebsiteSale


class EcommerceLimitAccess(WebsiteSale):

    @http.route([
            '/shop',
            '/shop/page/<int:page>',
            '/shop/category/<model("product.public.category"):category>',
            '/shop/category/<model("product.public.category"):category>/page/<int:page>'
        ], type='http', auth="user", website=True)
    def shop(self, page=0, category=None, search='', **post):
        if request.env.user.partner_id.has_ecommerce_access:
            return super(EcommerceLimitAccess, self).shop(page=page, category=category, search=search, **post)
        else:
            return request.render('custom_contacts.shop_page_not_allowed', {})
