# -*- coding: utf-8 -*-

from flectra import models, fields, api
from flectra.exceptions import Warning


class Partner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    fax = fields.Char(string="Fax", )
    contact_type_id = fields.Many2one(comodel_name="client.type", string="Type of Client", )
    nace_code_id = fields.Many2one(comodel_name="nace.code", string="Nace Code", translate=True, )
    has_ecommerce_access = fields.Boolean(string="Has E-commerce Access?")
    transport_company_id = fields.Many2one(comodel_name="transport.company", string="Transport Company", )
    delivery_instruction = fields.Text(string="Delivery Instruction", required=False)
    client_external_code = fields.Char(string="External Client Number", required=False)
    client_name = fields.Char(string="Client Name", )
    client_address = fields.Char(string="Client Address", )
    work_order = fields.Char(string="Work Order", )
    activity_code = fields.Char(string="Activity Code", )
    activity_code_text = fields.Char(string="Activity Code Text", )
    profit_center = fields.Char(string="Profit Center", )
    district_manager_code = fields.Char(string="DM Code", )
    district_manager_name = fields.Char(string="DM Name", )
    status = fields.Char(string="Status", )
    business_unit = fields.Char(string="Business Unit", )
    juridical_entity = fields.Char(string="Juridical Entity", )
    zetel = fields.Char(string="Zetel", )
    client_description = fields.Text(string="Client Description", )
    contract_start_date = fields.Date(string="Contract Start Date", required=False)
    contract_end_date = fields.Date(string="Contract End Date", required=False)
    invoice_to_email = fields.Char(string="Invoice to Email", required=False)
    invoice_edi = fields.Boolean(string="Invoice EDI")
    invoice_ftp_address = fields.Char(string="FTP Server", required=False)
    invoice_username = fields.Char(string="Username", required=False)
    invoice_password = fields.Char(string="Password", required=False)
    invoice_ftp_port = fields.Char(string="Port", required=False)
    order_to_email = fields.Char(string="Order to Email", required=False)
    order_edi = fields.Boolean(string="Order EDI")
    order_ftp_address = fields.Char(string="FTP Server", required=False)
    order_username = fields.Char(string="Username", required=False)
    order_password = fields.Char(string="Password", required=False)
    order_ftp_port = fields.Char(string="Port", required=False)
    block_code = fields.Selection(string="Block Code", selection=[('not_blocked', '[1] Not Blocked'),
                                                                  ('temp_blocked', '[2] Temporarily Blocked'),
                                                                  ('conditional_blocked', '[3] Conditional Blocked'),
                                                                  ('always_blocked', '[4] Always Blocked'),
                                                                  ], required=False)
    conditional_block_reasons = fields.Selection(string="Conditional Block Reasons", selection=[('issued', 'Issued Amount >'),
                                                                                                ('due_days', 'Due Days >')])
    blocking_amount = fields.Float(string="Amount", required=False)
    not_completed = fields.Float(string="Not Completed", compute="_compute_not_completed")
    total_amounts = fields.Float(string="Total amounts", compute="_compute_not_completed")
    total_due = fields.Float(string="Total Due", compute="_compute_not_completed")
    country_id = fields.Many2one(default=lambda s: s.env.ref('base.be'))
    company_type = fields.Selection(selection=[('company', 'Company'), ('person', 'Individual'), ], )
    is_company = fields.Boolean(default=True, )
    is_transportation_company = fields.Boolean(string="Is Transport Company", )
    gln_code = fields.Char(string="GLN Code", size=13, )
    is_blocked = fields.Boolean(string="Is Blocked?", compute="_compute_is_blocked")

    print_invoice = fields.Boolean(string="Print Invoice")
    email_invoice = fields.Boolean(string="Email Invoice")
    edi_invoice = fields.Boolean(string="EDI Invoice")

    @api.one
    @api.depends('block_code', 'conditional_block_reasons')
    def _compute_is_blocked(self):
        if self.block_code in ['always_blocked', 'temp_blocked']:
            self.is_blocked = True
        elif self.block_code == 'conditional_blocked':
            if self.conditional_block_reasons == 'issued' and self.total_due >= self.blocking_amount:
                self.is_blocked = True
            elif self.conditional_block_reasons == 'due_days' and self.total_due >= self.blocking_amount:
                self.is_blocked = True
        elif self.block_code == 'not_blocked':
            self.is_blocked = False
        else:
            self.is_blocked = False


    @api.one
    @api.depends('sale_order_ids')
    def _compute_not_completed(self):
        self.not_completed = sum([invoice.amount_total for invoice in self.invoice_ids])
        self.total_due = sum([invoice.residual for invoice in self.invoice_ids])
        self.total_amounts = sum([self.not_completed, self.total_invoiced])


class NaceCode(models.Model):
    _name = 'nace.code'
    _rec_name = 'name'
    _description = 'Nace Code'

    name = fields.Char(string="Description", required=True, translate=True, )
    code = fields.Char(string="Code", required=True, )

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.name and record.code:
                result.append((record.id, '[' + record.code + '] ' + record.name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        res = self.search(['|', ('name', operator, name), ('code', operator, name)]).name_get()
        return res


class ClientType(models.Model):
    _name = 'client.type'
    _rec_name = 'name'
    _description = 'Type of Client'

    name = fields.Char(string="Description", required=True, )
    code = fields.Char(string="Code", required=True, )

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.name and record.code:
                result.append((record.id, '[' + record.code + '] ' + record.name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        res = self.search(['|', ('name', operator, name), ('code', operator, name)]).name_get()
        return res


class TransportCompany(models.Model):
    _name = 'transport.company'
    _rec_name = 'name'
    _description = 'Transport Company'

    name = fields.Char(string="Name", required=True)


class CityZip(models.Model):
    _name = 'city.zip'
    _rec_name = 'name'
    _description = 'Cities Postal Codes'

    name = fields.Char(string="Postal Code", required=True)
    city_nl = fields.Char(string="City NL", required=False)
    city_fr = fields.Char(string="City FR", required=False)
    country_id = fields.Many2one(comodel_name="res.country", string="Country", required=False)

    _sql_constraints = [
        ('uniq_name', 'unique(name)', 'The postal code you are trying to add is already there!')
    ]


class AccountInvoice(models.Model):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    print_invoice = fields.Boolean(string="Print Invoice", related="partner_id.print_invoice")
    email_invoice = fields.Boolean(string="Email Invoice", related="partner_id.email_invoice")
    edi_invoice = fields.Boolean(string="EDI Invoice", related="partner_id.edi_invoice")
